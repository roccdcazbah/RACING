SFX = {}

SFX.engineloop = love.audio.newSource("SFX/engineloop.wav","static")
SFX.boost = love.audio.newSource("SFX/boost.wav","static")
SFX.longboost = love.audio.newSource("SFX/longboost.wav","static")
SFX.longboost:setPitch(2)
SFX.boost:setPitch(2)
SFX.ready = love.audio.newSource("SFX/ready.wav","static")
SFX.ready:setLooping(false)
SFX.go = love.audio.newSource("SFX/go.wav","static")
SFX.click = love.audio.newSource("SFX/click.wav","static")
SFX.slider = love.audio.newSource("SFX/slider.wav","static")
SFX.slider:setLooping(false)