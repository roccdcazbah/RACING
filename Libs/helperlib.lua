function math.dot(a, b)
	return a[1]*b[1] + a[2]*b[2] + (a[3] or 0)*(b[3] or 0)
end

function math.clamp(min, val, max)
    return math.max(min, math.min(val, max));
end

function math.normalize(x,y) 
    local l=(x*x+y*y)^.5 
    if l==0 then 
        return 0,0,0 
    else 
        return x/l,y/l,l 
    end 
end

function math.round(num, numDecimalPlaces)
    local mult = 10^(numDecimalPlaces or 0)
    return math.floor(num * mult + 0.5) / mult
  end

function INIT_GRAPHICS()
    love.graphics.setDefaultFilter("nearest","nearest")
    love.graphics.setBackgroundColor(0.05,0,0.1)
    font = love.graphics.newFont("Graphics/P2P.ttf",13)
    trackCanvas = love.graphics.newCanvas(1024,1024)
    mainCanvas = love.graphics.newCanvas(500,400)
end

function math.dist(x1,y1, x2,y2) return ((x2-x1)^2+(y2-y1)^2)^0.5 end

function bb(x1,y1,w1,h1, x2,y2,w2,h2)
    return x1 < x2+w2 and
           x2 < x1+w1 and
           y1 < y2+h2 and
           y2 < y1+h1
  end

function math.pointInCircle(x,y,circleX,circleY,circleRadius)
	return (x-circleX)^2 + (y - circleY)^2 < circleRadius^2
end