p3d = {}
p3d.w,p3d.h = 500,400
function p3d:load()
    p3d.plane = love.graphics.newImage("Graphics/ceiling.png")
    p3d.x,p3d.y = 0,0
    p3d.defaultScale = .06
    p3d.scale = .07
    p3d.rotation = 0
    p3d.dx,p3d.dy = 0,100

    love.graphics.setCanvas(trackCanvas)
    love.graphics.draw(self.plane,0,0)
    love.graphics.setCanvas()

end

function p3d:draw()
    for y=1,self.h do
        love.graphics.setScissor(0,y,self.w,1)
        love.graphics.draw(trackCanvas,self.w/2+self.dx,self.h/2+self.dy,self.rotation,y*self.scale,y*self.scale,self.x,self.y)
        love.graphics.setScissor()
    end
end

function p3d:updatePosition(x,y)
    self.x = x
    self.y = y
end

function p3d:updateRotation(r)
    self.rotation = r
end

function p3d:adjustRotation(r)
    self.rotation = self.rotation + r
end

function p3d:getPosition()
    return self.x,self.y
end

function p3d:getRotation()
    return self.rotation
end

function p3d:setScale(s)
    self.scale = s
end

function p3d:setPlaneSprite(imageData)
    p3d.plane = imageData
end