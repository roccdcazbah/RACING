fysix = {}

fysix.collisionObjects = {}

function fysix:load()
    G_WORLD = love.physics.newWorld(0,0,true)
    local function beginContact(a,b,coll)
        aData = a:getUserData()
        bData = b:getUserData()
        if aData.name == "SAND" then 
            player.onGrass = true
        end
        if aData.name == "CHECK" and aData.active then player.checkpoints = player.checkpoints + 1 end
    end

    local function endContact(a,b,coll)
        aData = a:getUserData()
        bData = b:getUserData()
        if aData.name == "SAND" then 
            player.onGrass = false
        end
        if aData.name == "GOAL" and player.checkpoints >= G_currentRace.map.checkpoints then 
            for i,v in ipairs(fysix.collisionObjects) do
                v.userdata.active = true
            end
            G_currentRace.laptimes[player.lap] = tostring(G_currentRace.min .. ":" .. G_currentRace.sec) 
            player.lap = player.lap + 1
            player.checkpoints = 0
            G_currentRace.timer = 0

            if player.lap > G_currentRace.map.laps then G_currentRace.finished = true end
        end
        if aData.name == "CHECK" then aData.active = false end
    end

    local function preSolve(a,b,coll)
        
    end

    local function postSolve(a,b,coll)
        
    end

    G_WORLD:setCallbacks(beginContact,endContact,preSolve,postSolve)

end

function fysix:update(dt)
    G_WORLD:update(dt)
end

function fysix:draw()

end