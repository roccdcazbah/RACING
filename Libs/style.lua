COLORS = {
    RED = {1,0.2,0.2},
    GRAY = {0.4,0.4,0.4},
    WHITE = {1,1,1},
    BLACK = {0,0,0},
    ELEC = {0,0.9,1},
    PINK = {1,0,0.7}
}