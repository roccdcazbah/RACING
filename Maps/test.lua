map_data = {
    laps = 3,
    checkpoints = 4,

    load = function()
    -- VÄGGAR
    table.insert(fysix.collisionObjects,Wall(240,220,500,420,{name="WALL",active=true})) -- mitten
    table.insert(fysix.collisionObjects,Wall(0,0,10,1024,{name="WALL",active=true})) -- vänster
    table.insert(fysix.collisionObjects,Wall(0,1024,1024,10,{name="WALL",active=true})) -- ner
    table.insert(fysix.collisionObjects,Wall(1024,0,10,1024,{name="WALL",active=true})) -- höger
    table.insert(fysix.collisionObjects,Wall(0,0,1024,10,{name="WALL",active=true})) -- upp
    -- SAND
    table.insert(fysix.collisionObjects,Wall(0,0,120,1024,{name="SAND",active=true},true)) -- vänster
    table.insert(fysix.collisionObjects,Wall(120,0,910,70,{name="SAND",active=true},true)) -- upp
    table.insert(fysix.collisionObjects,Wall(870,80,140,940,{name="SAND",active=true},true)) -- höger
    table.insert(fysix.collisionObjects,Wall(120,750,740,280,{name="SAND",active=true},true)) -- upp
    table.insert(fysix.collisionObjects,Wall(200,160,600,530,{name="SAND",active=true},true)) -- mitten
    -- MÅL
    table.insert(fysix.collisionObjects,Wall(465,690,30,68,{name="GOAL",active=true},true))

    -- CHECKPOINTS
    table.insert(fysix.collisionObjects,Wall(200,690,30,68,{name="CHECK",active=true},true))
    table.insert(fysix.collisionObjects,Wall(770,690,30,68,{name="CHECK",active=true},true))
    table.insert(fysix.collisionObjects,Wall(200,80,30,68,{name="CHECK",active=true},true))
    table.insert(fysix.collisionObjects,Wall(770,80,30,68,{name="CHECK",active=true},true))
--
    end
}
return map_data