Anim = Class("Anim")

function Anim:initialize(t)
  self.data = t
  self.img    = t.img
  self.quad   = t.quad
  self.start  = t.start
  self.length = t.length
  self.speed  = t.speed
  self.timer  = 1 / self.speed
  self.frame = self.start
  self.offset = 0
  self.run = true
end

function Anim:update(dt)
  if self.run then
    self:runTimer(dt)
    self:runAnimation()
    self:loop()
    self:setQuadOffset()
  else
    self:stopAnimation()
  end
end

function Anim:draw(x,y,direction)
  if direction == -1 then x = x+self.data.offsetX end
  love.graphics.draw(self.img,self.quad,x,y,0,direction,1)
end

function Anim:resetTimer()
  self.timer = 1 / self.speed
end

function Anim:reset()
  self.frame = self.start
  self:setQuadOffset()
end

function Anim:loop()
  if self.frame >= self.length then
    self:reset()
  end
end

function Anim:nextFrame()
  self.frame = self.frame + 1
end

function Anim:prevFrame()
  self.frame = self.frame + -1
end

function Anim:setQuadOffset()
  self.offset = self.data.offsetX * self.frame
  self.quad:setViewport(self.offset,0,self.data.offsetX,self.data.offsetY)
end

function Anim:runAnimation()
  if self.timer <= 0 then
    self:resetTimer()
    self:nextFrame()
  end
end

function Anim:stopAnimation()
  self:reset()
  self:resetTimer()
end

function Anim:runTimer(dt)
  self.timer = self.timer - dt
end

function Anim:go(boolean)
  self.run = boolean
end

function Anim:setStart(frame)
  self.start = frame
end

function Anim:setLength(length)
  self.length = length
end

function Anim:setSpeed(speed)
  self.speed = speed
end
