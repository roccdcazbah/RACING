GuiFrame = Class("GuiFrame")

function GuiFrame:initialize(effects,flyInSpeed,flyOutSpeed)
    self.effects = effects
    self.elements = {}
    self.flyInIndex = 1
    self.flyOutIndex = 1
    self.transition = "in"
    self.clickedElement = nil
    self.hoveredElement = nil
    self.flyInSpeed = flyInSpeed or 3000
    self.flyOutSpeed = flyOutSpeed or 5000
end

function GuiFrame:addElement(element,id)
    table.insert(self.elements,{id = id,element=element})
end

function GuiFrame:getElement(id)
    for i,v in ipairs(self.elements) do
        if v.id == id then
            return v.element
        end
    end
end

function GuiFrame:draw()
    for i,v in ipairs(self.elements) do
        v.element:draw()
    end
end

function GuiFrame:update(dt)
    for i,v in ipairs(self.elements) do
        v.element:update(dt)
        if v.element.hover then self.hoveredElement = v.element end
    end
    
    if self.effects then
        if self.transition == "in" then
            self:flyIn(dt,self.flyInSpeed)
        elseif self.transition == "out" then
            self:flyOut(dt,self.flyOutSpeed) 
        end
    end 

    function love.mousepressed(x,y,button)
        for i,v in ipairs(self.elements) do
            if button == 1 then 
                if v.element.hover and v.element.onClick then
                    SFX.click:play()
                    if v.element.triggerFly then
                        self.transition = "out"
                    end
                    self.clickedElement = v.element
                end
            end
        end
    end
end

function GuiFrame:flyIn(dt,speed)
    local elementCount = #self.elements
    local currentElement = self.elements[self.flyInIndex].element

    local function Fly(element)
        if element.x > element.originX then
            element.x = element.x - speed*dt
        else
            if self.flyInIndex < elementCount then
                self.flyInIndex = self.flyInIndex + 1
            end
            element.x = element.originX
        end
    end

    Fly(currentElement)
end

function GuiFrame:flyOut(dt,speed)
    local elementCount = #self.elements
    local currentElement = self.elements[self.flyOutIndex].element

    local function Fly(element)
        if element.x > -500 then
            element.x = element.x - speed*dt
        else
            if self.flyOutIndex < elementCount then
                self.flyOutIndex = self.flyOutIndex + 1
            else
                self.clickedElement:onClick()
            end
        end
    end

    Fly(currentElement)
end

GuiMenuBox = Class("GuiMenuBox")

function GuiMenuBox:initialize(x,y,w,h)
    self.parentState = GState:getCurrentState()
    self.originX,self.originY = x,y
    self.x,self.y = x,y
    self.w,self.h = w,h
    self.text = " "
    self.hover = false
    self.inPlace = false
    self.textScale = 1
    self.align = "center"
    self.textOffset = 0
    self.triggerFly = true
end

function GuiMenuBox:setCustomFunction(func)
    self.onClick = function()
        func()
    end
end

function GuiMenuBox:setTriggerFly(enable)
    self.triggerFly = enable
end

function GuiMenuBox:setTextScale(s)
    self.textScale = s
end

function GuiMenuBox:setText(text)
    self.text = text
end

function GuiMenuBox:setTextOffset(o)
    self.textOffset = o
end

function GuiMenuBox:setAlign(a)
    self.align = a
end

function GuiMenuBox:setX(x)
    self.x = x
end

function GuiMenuBox:draw()
    love.graphics.setColor(COLORS.ELEC)
        love.graphics.rectangle("fill",self.x,self.y,self.w,self.h)
    love.graphics.setColor(COLORS.PINK)
        love.graphics.setLineWidth(2)
            love.graphics.rectangle("line",self.x,self.y,self.w,self.h)
        love.graphics.setLineWidth(1)
        if not self.hover then
            love.graphics.setColor(COLORS.WHITE)
        else
            love.graphics.setColor(COLORS.BLACK)
        end
    love.graphics.printf(self.text,self.x+self.textOffset,self.y+5,self.w,self.align,0,self.textScale,self.textScale)
    love.graphics.setColor(1,1,1)
end

function GuiMenuBox:update(dt)
    if self.x ~= self.originX then
        self.inPlace = false
    else
        self.inPlace = true
    end

    local mX,mY = love.mouse.getPosition()

    if bb(self.x,self.y,self.w,self.h,mX,mY,1,1) then
        self.hover = true
    else
        self.hover = false
    end

end

GuiSlider = Class("GuiSlider")

function GuiSlider:initialize(x,y,defaultvalue,maxvalue)
    self.originX,self.originY = x,y
    self.x,self.y = 500,y
    self.ballX,self.ballY = x,y
    self.currentSize = 0
    self.ballSize = 8
    self.value = defaultvalue or 0
    self.maxvalue = maxvalue or 0
    self.hover = false
    self.grabbed = false
    self.inPlace = false
end

function GuiSlider:setValue(v)
    self.value = v
end

function GuiSlider:getValue()
    return self.value 
end

function GuiSlider:setBall(x)
    self.ballX = x
end

function GuiSlider:update(dt)
    if self.x <= self.originX then
        self.inPlace = true
    end

    if GState:getCurrentState().stateData.guiFrame.transition == "out" then
        if self.currentSize > -1 then self.currentSize = self.currentSize - 30*dt end
    else
        if self.currentSize < self.ballSize then
            self.currentSize = self.currentSize + 40*dt 

        end
    end

    local mx,my = love.mouse.getPosition()

    if math.pointInCircle(mx,my,self.ballX,self.ballY,self.ballSize) then
        self.hover = true
    else
        self.hover = false
    end

    if self.hover and love.mouse.isDown(1) then
        if not self.grabbed then
            SFX.slider:play()
        end
        self.grabbed = true
    end
    
    if not love.mouse.isDown(1) then
        self.grabbed = false
    end

    if self.grabbed then
        self.ballX = math.clamp(self.x,mx,self.x+200)
        self:setValue((self.ballX-self.x)/200)
    end
end

function GuiSlider:draw()
    love.graphics.line(self.x,self.y,self.x+200,self.y)
    if self.hover then 
        love.graphics.setColor(COLORS.ELEC)
    else
        love.graphics.setColor(COLORS.WHITE)
    end
    
    if self.currentSize > 0 then
        love.graphics.circle("fill",self.ballX or 500,self.ballY,self.currentSize)
        love.graphics.setColor(COLORS.PINK)
        love.graphics.circle("line",self.ballX or 500,self.ballY,self.currentSize)
        love.graphics.setColor(COLORS.WHITE)
    end
end

GuiLabel = Class("GuiLabel")

function GuiLabel:initialize(x,y,text)
    self.originX,self.originY = x,y
    self.x,self.y = 500,y
    self.text = text
    self.scale = 1
    self.color = COLORS.WHITE
end

function GuiLabel:update(dt)

end

function GuiLabel:draw()
    love.graphics.setColor(self.color)
        love.graphics.print(self.text,self.x,self.y,0,self.scale,self.scale)
    love.graphics.setColor(COLORS.WHITE)
end

function GuiLabel:setScale(s)
    self.scale = s
end

function GuiLabel:setText(t)
    self.text = t
end

function GuiLabel:setColor(color)
    self.color = color
end