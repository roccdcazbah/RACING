Map = Class("Map")

function Map:initialize(map)
    self.checkpoints = 4
    self.laps = 3

    self.map = require("Maps/" .. map)
    self.map:load()
end

function Map:update(dt)

end

function Map:draw()

end