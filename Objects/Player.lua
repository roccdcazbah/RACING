Player = Class("Player")

function Player:initialize(x,y,rot)
    self.body = love.physics.newBody(G_WORLD,x,y,"dynamic")
    self.shape = love.physics.newCircleShape(2)
    self.fixture = love.physics.newFixture(self.body,self.shape)
    self.defaultDamping = 3
    self.body:setLinearDamping(self.defaultDamping)
    self.body:setMass(0.2)
    self.fixture:setUserData("PLAYER")
    self.x = x
    self.y = y
    self.xvel = 0
    self.yvel = 0
    self.vel = 0
    self.acc = 200  
    self.defaultFriction = 2
    self.friction = self.defaultFriction
    self.speed = 0
    self.defaultMaxSpeed = {pos=100,neg=-40}
    self.maxSpeed = {pos=100,neg=-40}
    self.rotation = rot or 0
    self.defaultRotationSpeed = 1.5
    self.rotationSpeed = 1.5
    self.cols = {}
    self.len = 0

    -- underlag
    self.onGrass = false

    -- extra
    self.spinTime = 1
    self.spinCount = 0

    -- slide stuff
    self.slideinitialAngle = 0
    self.turboCount = 0
    self.inputDirection = 0
    self.slideDirection = 0
    self.slideInitialVelocity = {x=0,y=0}

    self.boostFailed = false
    self.canBoost = false
    self.successfulBoosts = 0
    self.sliding = false

    -- gfx
    self.sprite = love.graphics.newImage("Graphics/car.png")
    self.spriteW,self.spriteH = self.sprite:getWidth(),self.sprite:getHeight()
    self.defaultSpriteScale = 3
    self.spriteScale = 3.5
    self.grassIMG = love.graphics.newImage("Graphics/grass.png")
    self.fumeIMG = love.graphics.newImage("Graphics/fume.png")
    self.fumes1 = love.graphics.newParticleSystem(self.fumeIMG,32)
    self.fumes2 = love.graphics.newParticleSystem(self.fumeIMG,32)

    self.fumes1:setParticleLifetime(0.5,0.6)
    self.fumes1:setLinearAcceleration(-20,0,100,1000)
    self.fumes1:setColors(0.2,0.2,0.2,0.2,0.6,0.6,0.6,0.2)
    self.fumes1:setSizes(1,2,4) 
    self.fumes2:setParticleLifetime(0.5,0.6)
    self.fumes2:setLinearAcceleration(-20,0,100,1000)
    self.fumes2:setColors(0.2,0.2,0.2,0.2,0.6,0.6,0.6,0.2)
    self.fumes2:setSizes(1,2,4)

    self.grassSystem = love.graphics.newParticleSystem(self.grassIMG,32)
    self.grassSystem:setParticleLifetime(0.2,0.4)
    self.grassSystem:setLinearAcceleration(-500,-100,0,-500)
    self.grassSystem:setColors(0,1,0,1,0,0.5,0,1)
    self.grassSystem:setSizes(0.25,0.50,1)
    self.grassSystem:setRotation(0,math.pi)

    self.skidmarkCanvasBuffer = love.graphics.newCanvas(16,16)
    self.skidmarkCanvas = love.graphics.newCanvas(1024,1024)

    love.graphics.setCanvas(self.skidmarkCanvasBuffer)
    love.graphics.setColor(0,0,0)
        love.graphics.rectangle("fill",0,0,1,1)
        love.graphics.rectangle("fill",3,0,1,1)
    love.graphics.setColor(1,1,1)
    love.graphics.setCanvas()

    self.animData = {
        img = love.graphics.newImage("Graphics/car2.png"),
        quad = love.graphics.newQuad(0,0,1458/6,135,1458,135),
        offsetX = 1458/6,offsetY=135,
        start=1,length=6,speed=20
    }
    self.anim = Anim(self.animData)
    --RACE

    self.lap = 1
    self.checkpoints = 0

    -- INIT
    p3d:updatePosition(self.x,self.y)
    p3d:updateRotation(self.rotation)

end

function Player:update(dt)
    self.anim.frame = 1
    love.audio.play(SFX.engineloop)
    local enginePitch = 1+self.speed/100
    SFX.engineloop:setPitch(enginePitch)
    self:input(dt)
    self:slideAdjustRotationalSpeed(dt)
    self:speedoMeter(dt)
    self:physics(dt)
    self:updateParticleSystems(dt)
    p3d:updatePosition(self.x,self.y)

    p3d:updateRotation(self.rotation)

    if self.sliding and self.slideDirection ~= 0 then self.anim.frame = 5 end    
    self.anim:setQuadOffset()
    --self.anim:runTimer(dt)
    --self.anim:runAnimation()
    --self.anim:setQuadOffset()
   -- self.anim:update(dt)
end

function Player:draw()
    --love.graphics.draw(self.sprite,p3d.w/2-(self.spriteW/2*self.spriteScale),p3d.h-(self.spriteH*self.spriteScale)-90,0,self.spriteScale,self.spriteScale)
    
    local animDir = 1

    if self.inputDirection == 0 then animDir = 1 else
        animDir = self.inputDirection
    end

    if self.sliding then 
        if self.slideDirection ~= 0 then animDir = self.slideDirection else
            self.animDir = 1
        end
    end

    self.anim:draw(SW/2-120,SH/2-30,animDir)
    
    if self.vel > -1 then
        love.graphics.draw(self.fumes1,p3d.w/2-8,p3d.h-(self.spriteH*self.spriteScale)+40,0,4,4)
        love.graphics.draw(self.fumes2,p3d.w/2+8,p3d.h-(self.spriteH*self.spriteScale)+40,0,4,4)
    end
    
    if self.onGrass then
        love.graphics.draw(self.grassSystem,p3d.w/2-40,p3d.h-(self.spriteH*self.spriteScale),0,4,4)
        love.graphics.draw(self.grassSystem,p3d.w/2+40,p3d.h-(self.spriteH*self.spriteScale),0,-4,4)
    end
    
    local BarW = 200
    local BarH = 5
    local BarX,BarY = p3d.w/2-BarW/2,380
    local maxWidth = 70

    love.graphics.rectangle("line",BarX,BarY,BarW,BarH)
    love.graphics.setColor(self.speed/100,1-self.speed/100,0)
        love.graphics.rectangle(
            "fill",
            (BarX)+1,
            (BarY+1),
            (self.speed/200)*maxWidth,
            BarH-2
        )
    love.graphics.setColor(1,1,1)
    
    love.graphics.rectangle("line",BarX,BarY+10,BarW,BarH)
    love.graphics.line(BarX+(BarW/2),BarY+10,BarX+(BarW/2),BarY+15)
    love.graphics.setColor(1-self.turboCount/500,self.turboCount/200,0)

        love.graphics.rectangle(
            "fill",
            (BarX)+1,
            (BarY+11),
            ((self.turboCount/100)*maxWidth),
            BarH-2
        )
    love.graphics.setColor(1,1,1)


    -- Skidmarks
    if self.sliding then
        love.graphics.setCanvas(self.skidmarkCanvas)
            love.graphics.draw(self.skidmarkCanvasBuffer,self.body:getX()-2*math.cos(-self.rotation),self.body:getY()-2*math.sin(-self.rotation),-self.rotation)
        love.graphics.setCanvas()

        love.graphics.setCanvas(trackCanvas)
            love.graphics.draw(self.skidmarkCanvas,0,0)
        love.graphics.setCanvas()
    end

    local viewDistance = math.dist(self.x,self.y,200,200)

end

function Player:input(dt)
    self.inputDirection = 0
    if love.keyboard.isDown(G_CFG.BINDS.REVERSE) then
        self:rev(dt,-1)
    elseif love.keyboard.isDown(G_CFG.BINDS.ACCELERATE) then
        self:rev(dt,1)
    end

    if love.keyboard.isDown(G_CFG.BINDS.TURN_LEFT) then
        self.inputDirection = 1
        self.rotation = self.rotation + self.rotationSpeed*dt

        if self.anim.frame < self.anim.length then
            self.anim:nextFrame()
        end
    end

    if love.keyboard.isDown(G_CFG.BINDS.TURN_RIGHT) then
        self.inputDirection = -1
        self.rotation = self.rotation - self.rotationSpeed*dt
        self.anim:nextFrame()
    end

    if love.keyboard.isDown(G_CFG.BINDS.BRAKE) then
        self.sliding = true
        self:slide(dt)
    else
        self.sliding = false
    end

    function love.keypressed(key)
        if self.sliding and key == G_CFG.BINDS.BOOST and self.canBoost then
            self:boost(dt)
        end

        if key == "c" then
            local initVelX,initVelY = self.body:getLinearVelocity()
            self.slideInitialAngle = p3d:getRotation()
            self.slideInitialVelocity.x,self.slideInitialVelocity.y = initVelX,initVelY
            self.slideDirection = self.inputDirection
            self.friction = self.defaultFriction/4
        elseif key == "r" then love.load() 
        elseif key == "x" then 
            if not showHUD then showHUD = true
            else showHUD = false end
        elseif key == "m" then
            love.audio.setVolume(0)
        elseif key == "escape" then
            if not GState:getCurrentState().stateData.showGUI then
                GState:getCurrentState().stateData.showGUI = true
            else
                GState:getCurrentState().stateData.showGUI = false
            end
        end
    end

    function love.keyreleased(key)
        if key == "c" then
            self.slideInitialAngle = 0
            self.body:setLinearDamping(self.defaultDamping)
            self.slideDirection = 0
            self.friction = self.defaultFriction
            self.turboCount = 0
            self.boostFailed = false
            self.successfulBoosts = 0
        end
    end
end

function Player:resetTurbo()

end

function Player:turboMeter(dt)
        if self.sliding and self.turboCount < 275  and self.slideDirection ~= 0 then
            self.turboCount = self.turboCount + 310*dt
            if self.turboCount > 156 then
                self.canBoost = true
            end
        else
            self.canBoost = false
            self.boostFailed = true
            self.turboCount = 0
            self.succesfulBoosts = 0
        end
end

function Player:boost(dt)
    self.body:setLinearVelocity(0,0)

    if self.successfulBoosts < 2 then
        self.body:applyLinearImpulse(math.sin(self.rotation-(0.5*self.slideDirection))*-self.turboCount/6,math.cos(self.rotation-(0.5*self.slideDirection))*-self.turboCount/6)
        self.vel = 190
    else
        self.vel = 400
        self.maxSpeed.pos = 200
        self.body:applyLinearImpulse(math.sin(self.rotation-(0.5*self.slideDirection))*-self.turboCount/3,math.cos(self.rotation-(0.5*self.slideDirection))*-self.turboCount/3)
    end

    self.sliding = false
    self.turboCount = 0
    self.successfulBoosts = self.successfulBoosts + 1

    if self.successfulBoosts >= 3 then
        love.audio.play(SFX.longboost)
    else
        love.audio.play(SFX.boost)
    end
end

function Player:slide(dt)
   if not self.boostFailed and self.successfulBoosts < 3 then self:turboMeter(dt) end

    self.rotation = self.rotation + ((0.6*self.slideDirection)*dt)
    self.xvel = -math.sin(self.rotation) * self.vel/4
    self.yvel = -math.cos(self.rotation) * self.vel/4

    if self.turboCount >= 280 then self.turboCount = 0 self.canBoost = false end
    if self.successfulBoosts == 3 then self.canBoost = false end
end

function Player:physics(dt)
    if self.onGrass then
        self.body:setLinearDamping(10) 
    else
        self.body:setLinearDamping(self.defaultDamping)
    end

    if self.sliding then
        self.body:setLinearDamping(self.defaultDamping/3)
    end


    -- Friction
    if self.vel > 0 then
        self.vel = self.vel * (1 - math.min(dt * self.friction, 1))
    else
        self.vel = self.vel * (1 - math.min(dt * self.friction*3, 1))
    end

    if not self.sliding then
        self.xvel = -math.sin(self.rotation) * self.vel
        self.yvel = -math.cos(self.rotation) * self.vel
    end
    
    self.body:applyForce(self.xvel,self.yvel)


    -- Physics
    self.x,self.y = self.body:getX(),self.body:getY()
end

function Player:slideAdjustRotationalSpeed(dt)
    if not self.sliding then
        self.rotationSpeed = math.clamp(1,self.defaultRotationSpeed*(self.speed)/100,self.defaultRotationSpeed)
    else
        if self.slideDirection == self.inputDirection then
            self.rotationSpeed = 1.9
        else
            self.rotationSpeed = 0.6
        end
    end  
end

function Player:speedoMeter(dt)
    local linearVelocityX,linearVelocityY = self.body:getLinearVelocity()
    self.speed = math.sqrt((linearVelocityX-dt)^2 + (linearVelocityY-dt)^2)
end

function Player:updateParticleSystems(dt)
    if self.vel > 0 then
        self.fumes1:setEmissionRate(self.vel)
        self.fumes2:setEmissionRate(self.vel)
        if self.onGrass then
            self.grassSystem:setEmissionRate(self.vel*10)
        end
    end
    
    self.fumes1:update(dt)
    self.fumes2:update(dt)
    if self.onGrass then
        self.grassSystem:update(dt)
    end
end

function Player:rev(dt,gear)
    if not self.sliding then
            newRevs = (self.acc*dt)*gear
            self.vel = math.clamp(self.maxSpeed.neg,self.vel + newRevs,self.maxSpeed.pos)
    end
end