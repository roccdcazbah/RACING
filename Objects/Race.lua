Race = Class("Race")

function Race:initialize(map)
    self.map = Map(map)
    self.status = "WAIT"
    self.timer = 0
    self.min = 0
    self.sec = 0

    self.laptimes = {"","",""}
    self.started = false
    self.finished = false
    p3d:updatePosition(player.x,player.y)
    p3d:updateRotation(player.rotation)
    -- init map
end
 
function Race:update(dt)
    self:runTimer(dt)

    if self.status == "RACE" then
        self.sec = math.round(self.timer,1)
        if self.sec >= 60 then 
            self.min = self.min + 1 
            self.sec = 0 
        end
    end

end

function Race:draw()

end

function Race:drawInfo()
    love.graphics.setFont(font)
    love.graphics.print(self.min .. ":" .. self.sec,SW/2-15,10)
    love.graphics.print("LAP: " .. player.lap .. "/" .. self.map.laps,SW/2-50,465)

    if self.finished then
        love.graphics.print("LAPTIMES",SW/2-100,60,0,2,2)
        love.graphics.print("1: " .. self.laptimes[1],180,155)
        love.graphics.print("2: " .. self.laptimes[2],180,170)
        love.graphics.print("3: " .. self.laptimes[3],180,185)
    end
end

function Race:runTimer(dt)
    self.timer = self.timer + dt
end

function Race:countdown(dt)
    if not self.start then
        if self.timer >= 1 and self.timer <= 2 then self.status = "READY" 
        elseif self.timer >= 2 and self.timer <= 3 then self.status = "SET" 
        elseif self.timer >= 3 then self.status = "GO" 
        end
        if self.timer >= 3 then self.status = "RACE" self.timer = 0 self.start = true end

        if self.timer >= 1 and self.timer < 1.1 then love.audio.play(SFX.ready) end
        if self.timer >= 2 and self.timer < 2.1 then love.audio.play(SFX.ready) end
        if self.status == "RACE" and self.timer < 0.2 then love.audio.play(SFX.go) end
    end
end

function Race:drawCountdown()
    if not self.start then
        love.graphics.setColor(0.2,0.2,0.2) 
        if self.status == "READY" then
            love.graphics.setColor(1,0,0)
        end
            love.graphics.circle("fill",SW/2-100,SH/2-130,30)
            love.graphics.setColor(1,1,1)
    
            love.graphics.setColor(0.2,0.2,0.2)
        if self.status == "SET" then
            love.graphics.setColor(1,0,0)
        end
            love.graphics.circle("fill",SW/2,SH/2-130,30)
            love.graphics.setColor(1,1,1)
            love.graphics.setColor(0.2,0.2,0.2)
        if self.status == "GO" then
            love.graphics.setColor(0,1,0)
        end
        love.graphics.circle("fill",SW/2+100,SH/2-130,30)
        love.graphics.setColor(1,1,1)
    end    
end