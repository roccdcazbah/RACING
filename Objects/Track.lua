Track = Class("Track")

function Track:initialize(x,y,w,h,type)
    self.x,self.y = x,y
    self.w,self.h = w,h

    self.body = love.physics.newBody(G_WORLD,self.x+self.w/2,self.y+self.h/2,"static")
    self.shape = love.physics.newRectangleShape(self.w,self.h)
    self.fixture = love.physics.newFixture(self.body,self.shape)
    self.fixture:setRestitution(0.2)
    self.fixture:setSensor(true)

    self.body:setUserData("TRACK")
end

function Track:update()

end

function Track:draw()
    love.graphics.setColor(1,0,0)
        love.graphics.setLineWidth(1)
        love.graphics.polygon("line", self.body:getWorldPoints(self.shape:getPoints()))
        love.graphics.setLineWidth(1)
    love.graphics.setColor(1,1,1)
end

