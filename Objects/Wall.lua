Wall = Class("Wall")

function Wall:initialize(x,y,w,h,userdata,sensor)
    self.x,self.y = x,y
    self.w,self.h = w,h
    self.userdata = userdata
    self.type = userdata.type
    self.sensor = sensor or false
    self.body = love.physics.newBody(G_WORLD,self.x+self.w/2,self.y+self.h/2,"static")
    self.shape = love.physics.newRectangleShape(self.w,self.h)
    self.fixture = love.physics.newFixture(self.body,self.shape)
    self.fixture:setRestitution(0.2)
    self.fixture:setUserData(self.userdata)
    self.fixture:setSensor(self.sensor)
end

function Wall:update()

end

function Wall:draw()
    love.graphics.setColor(1,0,0)
        love.graphics.setLineWidth(1)
        love.graphics.polygon("line", self.body:getWorldPoints(self.shape:getPoints()))
        love.graphics.setLineWidth(1)
    love.graphics.setColor(1,1,1)
end

