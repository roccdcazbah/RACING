local state = {}
	
	state.name = "CONTROLS"
	
	
	function state:load() 
		self.guiFrame = GuiFrame(true,6000,30000)
		self.assign = false
		self.assignTarget = nil
		self.assignKey = ""
        
        local backButton = GuiMenuBox(SW/2-100,119+250,200,30)

        backButton:setText("BACK")
		backButton:setX(500)
		backButton:setCustomFunction(function() GState:switch("MENU") end)

		local accAssignButton = GuiMenuBox(SW/2-110,50,100,23)
		accAssignButton:setText("EDIT")
		accAssignButton:setX(500)
		accAssignButton:setTriggerFly(false)
		accAssignButton:setTextScale(0.6)
		accAssignButton:setTextOffset(20)
		accAssignButton:setCustomFunction(function() end)

		local revAssignButton = GuiMenuBox(SW/2-110,80,100,23)
		revAssignButton:setText("EDIT")
		revAssignButton:setX(500)
		revAssignButton:setTriggerFly(false)
		revAssignButton:setTextScale(0.6)
		revAssignButton:setTextOffset(20)
		revAssignButton:setCustomFunction(function() end)

		local leftAssignButton = GuiMenuBox(SW/2-110,110,100,23)
		leftAssignButton:setText("EDIT")
		leftAssignButton:setX(500)
		leftAssignButton:setTriggerFly(false)
		leftAssignButton:setTextScale(0.6)
		leftAssignButton:setTextOffset(20)
		leftAssignButton:setCustomFunction(function() end)

		local rightAssignButton = GuiMenuBox(SW/2-110,140,100,23)
		rightAssignButton:setText("EDIT")
		rightAssignButton:setX(500)
		rightAssignButton:setTriggerFly(false)
		rightAssignButton:setTextScale(0.6)
		rightAssignButton:setTextOffset(20)
		rightAssignButton:setCustomFunction(function() end)

		local slideAssignButton = GuiMenuBox(SW/2-110,170,100,23)
		slideAssignButton:setText("EDIT")
		slideAssignButton:setX(500)
		slideAssignButton:setTriggerFly(false)
		slideAssignButton:setTextScale(0.6)
		slideAssignButton:setTextOffset(20)
		slideAssignButton:setCustomFunction(function() end)

		local boostAssignButton = GuiMenuBox(SW/2-110,200,100,23)
		boostAssignButton:setText("EDIT")
		boostAssignButton:setX(500)
		boostAssignButton:setTriggerFly(false)
		boostAssignButton:setTextScale(0.6)
		boostAssignButton:setTextOffset(20)
		boostAssignButton:setCustomFunction(function() end)

		local accelerateLabel = GuiLabel(SW/2,50,"ACCELERATE")
		accelerateLabel:setScale(0.7)
		local accelerateKey = GuiLabel(SW/2,63,"Key: " .. G_CFG.BINDS.ACCELERATE)
		accelerateKey:setScale(0.6)
		accelerateKey:setColor(COLORS.ELEC)

		local reverseLabel = GuiLabel(SW/2,80,"REVERSE")
		reverseLabel:setScale(0.7)
		local reverseKey = GuiLabel(SW/2,93,"Key: " .. G_CFG.BINDS.REVERSE)
		reverseKey:setScale(0.6)
		reverseKey:setColor(COLORS.ELEC)

		local leftLabel = GuiLabel(SW/2,110,"LEFT")
		leftLabel:setScale(0.7)
		local leftKey = GuiLabel(SW/2,123,"Key: " .. G_CFG.BINDS.TURN_LEFT)
		leftKey:setScale(0.6)
		leftKey:setColor(COLORS.ELEC)

		local rightLabel = GuiLabel(SW/2,140,"RIGHT")
		rightLabel:setScale(0.7)
		local rightKey = GuiLabel(SW/2,153,"Key: " .. G_CFG.BINDS.TURN_RIGHT)
		rightKey:setScale(0.6)
		rightKey:setColor(COLORS.ELEC)

		local slideLabel = GuiLabel(SW/2,170,"SLIDE")
		slideLabel:setScale(0.7)
		local slideKey = GuiLabel(SW/2,183,"Key: " .. G_CFG.BINDS.BRAKE)
		slideKey:setScale(0.6)
		slideKey:setColor(COLORS.ELEC)

		local boostLabel = GuiLabel(SW/2,200,"BOOST")
		boostLabel:setScale(0.7)
		local boostKey = GuiLabel(SW/2,213,"Key: " .. G_CFG.BINDS.BOOST)
		boostKey:setScale(0.6)
		boostKey:setColor(COLORS.ELEC)

		self.guiFrame:addElement(accAssignButton)
		self.guiFrame:addElement(revAssignButton)
		self.guiFrame:addElement(leftAssignButton)
		self.guiFrame:addElement(rightAssignButton)
		self.guiFrame:addElement(slideAssignButton)
		self.guiFrame:addElement(boostAssignButton)

		self.guiFrame:addElement(accelerateLabel)
		self.guiFrame:addElement(accelerateKey,"accKey")
		self.guiFrame:addElement(reverseLabel)
		self.guiFrame:addElement(reverseKey,"revKey")
		self.guiFrame:addElement(leftLabel)
		self.guiFrame:addElement(leftKey,"leftKey")
		self.guiFrame:addElement(rightLabel)
		self.guiFrame:addElement(rightKey,"rightKey")
		self.guiFrame:addElement(slideLabel)
		self.guiFrame:addElement(slideKey,"slideKey")
		self.guiFrame:addElement(boostLabel)
		self.guiFrame:addElement(boostKey,"boostKey")
        self.guiFrame:addElement(backButton)
	
	end

	function state:update(dt)
		accKey = self.guiFrame:getElement("accKey")
		revKey = self.guiFrame:getElement("revKey")
		leftKey = self.guiFrame:getElement("leftKey")
		rightKey = self.guiFrame:getElement("rightKey")
		slideKey = self.guiFrame:getElement("slideKey")
		boostKey = self.guiFrame:getElement("boostKey")

		accKey:setText(G_CFG.BINDS.ACCELERATE)
		revKey:setText(G_CFG.BINDS.REVERSE)
		leftKey:setText(G_CFG.BINDS.TURN_LEFT)
		rightKey:setText(G_CFG.BINDS.TURN_RIGHT)
		slideKey:setText(G_CFG.BINDS.BRAKE)
		boostKey:setText(G_CFG.BINDS.BOOST)

		self.guiFrame:update(dt)

		function love.keypressed(key)
			self.assignKey = key
			self.assign = false
		end

	end

	function state:draw()
		love.graphics.setFont(menuFont)
        love.graphics.setCanvas(mainCanvas)
        love.graphics.clear()
			self.guiFrame:draw()
		love.graphics.setCanvas()
		love.graphics.draw(mainCanvas,0,0)
	end

	function state:unload()

	end
return state