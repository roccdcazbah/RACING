local state = {}
	
	state.name = "CREDITS"
	
	
	function state:load() 
		self.guiFrame = GuiFrame(true)
        
        local backButton = GuiMenuBox(SW/2-100,119+250,200,30)

        backButton:setText("BACK")
		backButton:setX(500)
		backButton:setCustomFunction(function() GState:switch("MENU") end)
        self.guiFrame:addElement(backButton)
	
	end

	function state:update(dt)
		self.guiFrame:update(dt)
	end

	function state:draw()
		love.graphics.setFont(menuFont)
        love.graphics.setCanvas(mainCanvas)
        love.graphics.clear()
			self.guiFrame:draw()
		love.graphics.setCanvas()
		love.graphics.draw(mainCanvas,0,0)
	end

	function state:unload()

	end
return state