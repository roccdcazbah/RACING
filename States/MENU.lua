local state = {}
	
	state.name = "MENU"
	
	
	function state:load() 
		self.guiFrame = GuiFrame(true)
		self.nextState = ""

		local startButton = GuiMenuBox(SW/2-100,50+170,200,30)
		local settingsButton = GuiMenuBox(SW/2-100,50+210,200,30)
		local creditsButton = GuiMenuBox(SW/2-100,50+250,200,30)
		local quitButton = GuiMenuBox(SW/2-100,50+290,200,30)

		startButton:setText("RACE")
		startButton:setX(500)
		startButton:setCustomFunction(function() GState:switch("RACE") end)
		settingsButton:setText("SETTINGS")
		settingsButton:setX(500)
		settingsButton:setCustomFunction(function() GState:switch("SETTINGS") end)
		creditsButton:setText("CREDITS")
		creditsButton:setX(500)
		creditsButton:setCustomFunction(function() GState:switch("CREDITS") end)
		quitButton:setText("QUIT")
		quitButton:setX(500)
		quitButton:setCustomFunction(function() love.event.quit() end)
		
		self.guiFrame:addElement(startButton)
		self.guiFrame:addElement(settingsButton)
		self.guiFrame:addElement(creditsButton)
		self.guiFrame:addElement(quitButton)
	end

	function state:update(dt)
		self.guiFrame:update(dt)
	end

	function state:draw()
		love.graphics.setFont(menuFont)
        love.graphics.setCanvas(mainCanvas)
        love.graphics.clear()
			self.guiFrame:draw()
		love.graphics.setCanvas()
		love.graphics.draw(mainCanvas,0,0)
	end

	function state:unload()

	end
return state