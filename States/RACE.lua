local state = {}
	
	state.name = "RACE"
	
	
	function state:load()
		self.guiFrame = GuiFrame(true)

		local backButton = GuiMenuBox(SW/2-100,119+250,200,30)
		backButton:setText("QUIT")
		backButton:setX(500)
		backButton:setCustomFunction(function() GState:switch("MENU") end)
		self.guiFrame:addElement(backButton)

		local retryButton = GuiMenuBox(SW/2-100,119+210,200,30)
		retryButton:setText("RETRY")
		retryButton:setX(500)
		retryButton:setCustomFunction(function() GState:switch("RACE",true) end)
		self.guiFrame:addElement(retryButton)
		

		fysix.load()
		player = Player(446,723,-1.55)
		p3d:load()
		G_currentRace = Race("test")
		state.fadeOut = 0
		state.showGUI = false

		for i,v in ipairs(fysix.collisionObjects) do
			if not v.fixture:isDestroyed() then
			local data = v.fixture:getUserData()
			if data.name == "CHECK" then
				love.graphics.setColor(0.2,0.4,0,0.2)
			elseif data.name == "GOAL" then
				love.graphics.setColor(1,0.2,0.7,0.2)
			else
				love.graphics.setColor(0,0.6,0.6,0.2)
			end
			love.graphics.setCanvas(trackCanvas)
			love.graphics.polygon("fill",v.body:getWorldPoints(v.shape:getPoints()))
			love.graphics.setColor(1,1,1,0.2)
			love.graphics.polygon("line",v.body:getWorldPoints(v.shape:getPoints()))
			love.graphics.setColor(1,1,1,1)
			love.graphics.setCanvas()
			end
		end
	end

	function state:update(dt)
		if self.showGUI then
			function love.keypressed(key)
				if key == "escape" then self.showGUI = false end
			end
			self.guiFrame:update(dt)
		else
			if not G_currentRace.finish then
				G_currentRace:update(dt)
			else
				state.fadeOut = state.fadeOut + 0.5*dt
			end
	
			if G_currentRace.status ~= "RACE" then
				G_currentRace:countdown(dt)
			end
			if G_currentRace.status == "RACE" then
				player:update(dt)
				fysix:update(dt)
			end
		end
	end 

	function state:draw()

		love.graphics.setCanvas(mainCanvas)
    	love.graphics.clear()
    		p3d:draw()
    		player:draw()
		love.graphics.setCanvas()

		love.graphics.draw(mainCanvas,0,100)

		if G_currentRace.status ~= "RACE" then
			G_currentRace:drawCountdown()
		end
		
		G_currentRace:drawInfo()


		if self.showGUI then
			love.graphics.setColor(0,0,0,0.8)
				love.graphics.rectangle("fill",0,0,SW,SH)
			love.graphics.setColor(1,1,1,1)

			love.graphics.setFont(menuFont)
				self.guiFrame:draw()
			love.graphics.setFont(smallFont)
		end
	end

	function state:unload()
		for i,v in ipairs(fysix.collisionObjects) do
			if not v.fixture:isDestroyed() then
				v.fixture:destroy()
			end
		end
		G_WORLD:destroy()
		player = nil
		G_currentRace = nil
		collectgarbage()
	end

return state