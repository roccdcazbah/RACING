local state = {}
	
	state.name = "SETTINGS"
	
	
	function state:load() 
		self.guiFrame = GuiFrame(true)

		local controlsButton = GuiMenuBox(SW/2-100,119+25,200,30)
		controlsButton:setText("CONTROLS")
		controlsButton:setCustomFunction(function() GState:switch("CONTROLS") end)
		controlsButton:setX(500)
        local backButton = GuiMenuBox(SW/2-100,119+250,200,30)

        backButton:setText("BACK")
		backButton:setX(500)
		backButton:setCustomFunction(function() GState:switch("MENU") end)

		local mainVolumeLabel = GuiLabel(SW/2-60,70,"MAIN VOLUME")
		mainVolumeLabel:setScale(0.5)
		local mainVolumeSlider = GuiSlider(SW/2-100,100,0,100)
		mainVolumeSlider:setBall(mainVolumeSlider.originX+(love.audio.getVolume()*200))

		self.guiFrame:addElement(controlsButton)
		self.guiFrame:addElement(backButton)
		self.guiFrame:addElement(mainVolumeLabel)
		self.guiFrame:addElement(mainVolumeSlider,"mainVolumeSlider")
	
	end

	function state:update(dt)
		self.guiFrame:update(dt)

		-- MAIN VOLUME SLIDER
		local mainVolumeSlider = self.guiFrame:getElement("mainVolumeSlider")
		local globalVolumeValue = mainVolumeSlider:getValue()
		love.audio.setVolume(globalVolumeValue)
		
		--

	end

	function state:draw()
		love.graphics.setFont(menuFont)
        love.graphics.setCanvas(mainCanvas)
        love.graphics.clear()
			self.guiFrame:draw()
		love.graphics.setCanvas()
		love.graphics.draw(mainCanvas,0,0)
	end

	function state:unload()

	end
return state