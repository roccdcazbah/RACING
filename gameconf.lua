-- Default config file exists here
-- Any modifications to this file may cause the game to simply stop working


GAME_CONFIG = {
    BINDS = {
        ACCELERATE = "up",
        REVERSE = "down",
        TURN_LEFT = "left",
        TURN_RIGHT = "right",
        BRAKE = "c",
        BOOST = "z"
    },
    SOUND = {
        MAINVOLUME = 0.5
    }
}

return GAME_CONFIG