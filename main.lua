-- DEV LIBS
require "Libs/cupid"
Class = require "Libs/middleclass"
-- GAME LIBS
require "Libs/helperlib"
require "Libs/p3d"
require "Libs/GState"
require "Libs/physics"
require "Libs/SFX"
require "Libs/style"
-- GAME OBJECTS
require "Objects/Anim"
require "Objects/Player"
require "Objects/Wall"
require "Objects/Map"
require "Objects/Race"
require "Objects/Gui"

smallFont = love.graphics.newFont("Graphics/P2P.ttf",10)
menuFont = love.graphics.newFont("Graphics/P2P.ttf",20)

G_CFG = require "gameconf"

function love.load()
    -- KILL THE AUDIO EH?
    love.audio.setVolume(G_CFG.SOUND.MAINVOLUME)
    -- Init Graphics
    INIT_GRAPHICS()
    -- Init Libs
    GState:load()
    -- Init Globals << Bad shit
    G_currentRace = nil
    GState:switch("CONTROLS")
end

function love.update(dt)
    GState:update(dt)
end
 
function love.draw()
    love.graphics.setFont(smallFont)
    love.graphics.print("FPS: " .. love.timer.getFPS(),0,0)
    love.graphics.print("DT: " .. love.timer.getDelta(),0,13)
    GState:draw()
end

-- DEBUG SHIT

function DUMP_GLOBALS()

    local seen={}

    function dump(t,i)
	    seen[t]=true
	    local s={}
	    local n=0
	    for k in pairs(t) do
		    n=n+1 s[n]=k
	    end
	    table.sort(s)
	    for k,v in ipairs(s) do
		    print(i,v)
		    v=t[v]
		    if type(v)=="table" and not seen[v] then
			    dump(v,i.."\t")
		    end
	    end
    end
    
    dump(_G,"")
end